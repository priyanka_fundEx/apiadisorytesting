package com.apidoc.rest.clients;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap; 
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

public class ListUserClient 
{
	String appId, sessionid;
	public ListUserClient(String appId, String sessionId)
	{
		this.appId=appId;
		this.sessionid=sessionId;
	}
	public JSONObject run()  
	{
		JSONArray array=new JSONArray();
		JSONObject jObject=null; 
		List<Map<String, Integer>> list=new ArrayList<Map<String,Integer>>();
		Map<String,Integer> map = new HashMap<String,Integer>();
		try {
			JSONObject jsonObject=new JSONObject();
			jsonObject.put("appId", appId);
			jsonObject.put("sessionId", sessionid);
			System.out.println("JSON Object: "+jsonObject.toString());
			
			StringBuilder builder=new StringBuilder();
			try {
				URL url = new URL("http://localhost:8080/APIDocumentationForFE/rest/user/list");
				URLConnection con=url.openConnection();
				con.setDoOutput(true);
				con.setRequestProperty("Content-Type", "application/json");
				con.setConnectTimeout(50000);
				con.setReadTimeout(50000);
				
				OutputStreamWriter writer=new OutputStreamWriter(con.getOutputStream());
				writer.write(jsonObject.toString());
				writer.close();
				
				BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
				String line=br.readLine();
				while((line=br.readLine())!=""){
					builder.append(line);
					//line=br.readLine();
				}
				
				jObject=new JSONObject(builder.toString()); 
				
				System.out.println("ApiDOc REST Service for List User Invoked Successfully..");
				br.close();
			} catch (Exception e) {
				System.out.println("Error while calling ApiDOc REST List User Invoked Service");
				e.printStackTrace();
			}
			return jObject;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return jObject;
	}
}