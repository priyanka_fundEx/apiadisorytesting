package com.apidoc.rest.clients;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONObject;

public class CopyOfRegisterUserClient 
{
	String appId, sessionid;
	public CopyOfRegisterUserClient(String appId, String sessionId)
	{
		this.appId=appId;
		this.sessionid=sessionId;
	}
	public static void main(String[] args) 
	{ 
		JSONObject jObject;
		StringBuilder builder=new StringBuilder();
		String input = "";
		try {			
			input="{\"appId\": \"qwertyuiop12345\",\"secretKey\": \"asdfghjkl67890\"}";
			JSONObject jsonObject=new JSONObject(input);
			System.out.println("JSON Object: "+jsonObject);
			 
			try {				
				URL url=new URL("http://localhost:8080/APIDocumentationForFE/rest/user/create");
				URLConnection con=url.openConnection();
				con.setDoOutput(true);
				con.setRequestProperty("Content-Type", "application/json");
				con.setConnectTimeout(5000);
				con.setReadTimeout(5000);
				
				OutputStreamWriter writer=new OutputStreamWriter(con.getOutputStream());
				writer.write(jsonObject.toString());
				writer.close();
				
				BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
				String line=br.readLine();
				while(line!=null)
				{
					builder.append(line);
					line=br.readLine();
				}

				jObject=new JSONObject(builder.toString());
				
				String userId;
			    if(jObject.get("success").equals(true))
			    {
			    	userId=jObject.getString("userId");
			    	System.out.println("ApiDOc REST Service for LOGIN Invoked Successfully..");
			    }
				br.close();
				//return jObject;
			} catch (Exception e) {
				System.out.println("Error while calling ApiDOc REST LOGIN Service");
				e.printStackTrace();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		} 
	}
}