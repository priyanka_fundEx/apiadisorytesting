package com.apidoc.rest.clients;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection; 
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class SubscribeToFundClient 
{
	String appId, sessionId, userId, amfiiList;
	public SubscribeToFundClient(String appId, String sessionId, String userId, String amfiiList)
	{
		this.appId=appId;
		this.sessionId=sessionId;
		this.userId=userId;
		this.amfiiList=amfiiList;
	}
	public JSONObject run() 
	{
		JSONObject object=null;
		StringBuilder builder=new StringBuilder();
		try {
			
			JSONObject jsonObject=new JSONObject();
			jsonObject.put("appId", appId);
			jsonObject.put("sessionId", sessionId);
			jsonObject.put("userId", userId);

			JSONArray array=new JSONArray();
			
			String pattern= "[1-9]{1}[0-9]{5}";
			String amfii;
			boolean match=false;
		    List<String> dateLists=Arrays.asList(amfiiList.trim().split(","));
		    for(String str:dateLists)
		    {
		    	if(str.matches(pattern))
		    	{
		    		JSONObject obj=new JSONObject();
		    		obj.put("amfiiCode", str);
		    		array.put(obj);
		    	}
		    	else
		    		System.out.println("Amfii Code is not valis.");
		    }			
			
			jsonObject.put("mutualfund", array);
			System.out.println("INPUT JSON Object: "+jsonObject);
			
			try {
				URL url = new URL("http://localhost:8080/APIDocumentationForFE/rest/user/mutualfund/subscription/create");
				URLConnection con=url.openConnection();
				con.setDoOutput(true);
				con.setRequestProperty("Content-Type", "application/json");
				con.setConnectTimeout(50000);
				con.setReadTimeout(50000);
				
				OutputStreamWriter writer=new OutputStreamWriter(con.getOutputStream());
				writer.write(jsonObject.toString());
				writer.close();
				System.out.println("Output Strem Writer.");
				
				BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
				String line=br.readLine();
				while(line!=null){ 
					builder.append(line);
					line=br.readLine();
				}
				System.out.println("ApiDOc REST Service for Subscribe to fund Invoked Successfully.."+builder.toString());
				br.close();
				object=new JSONObject(builder.toString()); 
				return object;
			} catch (Exception e) {
				System.out.println("Error while calling ApiDOc REST Subscribe to fund Service: 	"+e.getMessage());
				e.printStackTrace();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return object;
	}
}