package com.apidoc.rest.clients;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONObject;
 

public class UpdateRiskProfileClient 
{
	String appId, sessionId, userId,riskProfile;
	public UpdateRiskProfileClient(String appId, String sessionId, String userId, String riskProfile)
	{
		this.appId=appId;
		this.sessionId=sessionId;
		this.userId=userId;
		this.riskProfile=riskProfile;
	}
	public JSONObject run()
	{
		JSONObject jObject=null; 
		try 
		{
			JSONObject jsonObject=new JSONObject();
			jsonObject.put("appId", appId);
			jsonObject.put("sessionId", sessionId);
			jsonObject.put("userId", userId);
			jsonObject.put("riskProfile", riskProfile);
			System.out.println("JSON Object: "+jsonObject.toString());
			
			StringBuilder builder=new StringBuilder();
			
			try {
				URL url = new URL("http://localhost:8080/APIDocumentationForFE/rest/user/riskprofile/update");
				URLConnection con=url.openConnection();
				con.setDoOutput(true);
				con.setRequestProperty("Content-Type", "application/json");
				con.setConnectTimeout(50000);
				con.setReadTimeout(50000);
				
				OutputStreamWriter writer=new OutputStreamWriter(con.getOutputStream());
				writer.write(jsonObject.toString());
				writer.close();
				
				BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
				String line=br.readLine();
				while(line!=null)
				{
					builder.append(line);
					System.out.println(line);
					line=br.readLine();
				}
 
				jObject=new JSONObject(builder.toString());
				
				
				System.out.println("User Updated Successfully");
				br.close();
				return jObject;
			} catch (Exception e) {
				System.out.println("Error while calling ApiDOc REST LOGIN Service");
				e.printStackTrace();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return jObject;
	}
}