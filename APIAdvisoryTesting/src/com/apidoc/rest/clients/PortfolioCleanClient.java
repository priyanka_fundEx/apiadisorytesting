package com.apidoc.rest.clients;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONObject;

public class PortfolioCleanClient 
{
	String appId, sessionId, userId, amfiiCode, folioNumber, date;
	public PortfolioCleanClient(String appId, String sessionId, String userId, String amfiiCode, String folioNumber, String date)
	{
		this.appId=appId;
		this.sessionId=sessionId;
		this.userId=userId; 
		this.amfiiCode=amfiiCode;
		this.folioNumber=folioNumber;
		this.date=date; 
	}
	public JSONObject run()
	{
		JSONObject object=null;
		StringBuilder builder=new StringBuilder();
		try {
			JSONObject jsonObject=new JSONObject();
			jsonObject.put("appId", appId);
			jsonObject.put("sessionId", sessionId);
			jsonObject.put("userId", userId);
			jsonObject.put("amfiiCode", amfiiCode);
			jsonObject.put("folioNumber", folioNumber);
			jsonObject.put("date", date);
			System.out.println("INPUT JSON Object: "+jsonObject);
			
			try {
				URL url = new URL("http://localhost:8080/APIDocumentationForFE/rest/user/portfolio/clean");
				URLConnection con=url.openConnection();
				con.setDoOutput(true);
				con.setRequestProperty("Content-Type", "application/json");
				con.setConnectTimeout(50000);
				con.setReadTimeout(50000);
				
				OutputStreamWriter writer=new OutputStreamWriter(con.getOutputStream());
				writer.write(jsonObject.toString());
				writer.close();
				System.out.println("Output Strem Writer.");
				
				BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
				String line=br.readLine();
				while(line!=null){
					builder.append(line);
					line=br.readLine();
				}
				object=new JSONObject(builder.toString());
				System.out.println("ApiDOc REST Service for Subscribe to fund Invoked Successfully..");
				br.close();
				return object;
			} catch (Exception e) {
				System.out.println("Error while calling ApiDOc REST Subscribe to fund Service");
				e.printStackTrace();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return object;
	}
}