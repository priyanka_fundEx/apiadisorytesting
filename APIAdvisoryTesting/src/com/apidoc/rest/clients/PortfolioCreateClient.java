package com.apidoc.rest.clients;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONArray;
import org.json.JSONObject;

public class PortfolioCreateClient 
{	// appId, sessionId, userId, amfiiCode, folioNumber, optionType, action, nav, units, date
	String appId, sessionId, userId, amfiiCode, folioNumber, nav, units, optionType, action, date;
	public PortfolioCreateClient(String appId, String sessionId, String userId, String amfiiCode, String folioNumber,
			 String optionType,	String action, String nav, String units,  String date)
	{
		this.appId=appId;
		this.sessionId=sessionId;
		this.userId=userId; 
		this.amfiiCode=amfiiCode;
		this.folioNumber=folioNumber;
		this.nav=nav;
		this.units=units; 
		this.optionType=optionType;
		this.action=action;
		this.date=date;
		
		this.userId=userId; 
		this.amfiiCode=amfiiCode;
	}
	public JSONObject run()
	{
		JSONObject object=null;

		StringBuilder builder=new StringBuilder(); 
		try {
			JSONObject jsonObject=new JSONObject();
			jsonObject.put("appId", appId);
			jsonObject.put("sessionId", sessionId);
			jsonObject.put("userId", userId);
			
			JSONArray array=new JSONArray();
			JSONObject ob=new JSONObject();
			ob.put("amfiiCode", amfiiCode);
			ob.put("folioNumber", folioNumber);
			ob.put("optionType", optionType);
			ob.put("nav", nav);
			ob.put("units", units);
			ob.put("action", action);
			ob.put("date", date);
			
			array.put(ob);
			jsonObject.put("transactions", array);
			System.out.println("INPUT JSON Object: "+jsonObject);
			
			try {
				URL url = new URL("http://localhost:8080/APIDocumentationForFE/rest/user/portfolio/create");
				URLConnection con=url.openConnection();
				con.setDoOutput(true);
				con.setRequestProperty("Content-Type", "application/json");
				con.setConnectTimeout(50000);
				con.setReadTimeout(50000);
				
				OutputStreamWriter writer=new OutputStreamWriter(con.getOutputStream());
				writer.write(jsonObject.toString());
				writer.close();
				System.out.println("Output Strem Writer.");
				
				BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
				String line=br.readLine();
				while(line!=null){
					builder.append(line);
					line=br.readLine();
				}
				
				br.close();
				object=new JSONObject(builder.toString());
				return object;
				} 
				catch (Exception e) {
				System.out.println("Error while calling ApiDOc REST Subscribe to fund Service");
				e.printStackTrace();
				}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	return object;
	}
}