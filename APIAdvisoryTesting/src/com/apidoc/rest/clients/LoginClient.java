package com.apidoc.rest.clients;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.function.Consumer;

import javax.validation.ValidationException;

import org.json.JSONObject;

public class LoginClient 
{
	String appId, secretKey; 
	
	public LoginClient(String appId, String secretKey)
	{
		this.appId=appId;
		this.secretKey=secretKey;
	}
	
	public JSONObject run() 
	{
		JSONObject jObject=null;
		String sessionId=null;
		StringBuilder builder=new StringBuilder();
		String input = "";
		try {
			JSONObject jsonObject=new JSONObject();
			if(appId!=null && secretKey!=null )
			{
				jsonObject.put("appId", appId);
				jsonObject.put("secretKey", secretKey);
				System.out.println("JSON Object: "+jsonObject);
			}
			else
				throw new ValidationException("Please Enter a valid Parameter.");
			try {
				URL url = new URL("http://localhost:8080/APIDocumentationForFE/rest/consumer/login");
				URLConnection con=url.openConnection();
				con.setDoOutput(true);
				con.setRequestProperty("Content-Type", "application/json");
				con.setConnectTimeout(50000);
				con.setReadTimeout(50000);
				
				OutputStreamWriter writer=new OutputStreamWriter(con.getOutputStream());
				writer.write(jsonObject.toString());
				writer.close();
				System.out.println("Output Strem Writer.");
				
				BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
				String line=br.readLine();
				while(line!=null){
					System.out.println(line);
					builder.append(line);
					line=br.readLine();
				}

				jObject=new JSONObject(builder.toString());
				/*Map<String,String> map = new HashMap<String,String>();
			    Iterator iter = jObject.keys();
			    while(iter.hasNext()){
			        String key = (String)iter.next();
			        String value = jObject.getString(key);
			        map.put(key,value);
			    }*/
			     
				//sessionId=jObject.getString("sessionId");
				System.out.println("ApiDOc REST Service for LOGIN Invoked Successfully..");
				
				if(jObject.equals(new JSONObject()))
					System.out.println("Empty JSON returned.");
				else if(jObject.equals("{}"))
					System.out.println("Enpty JSON returned. matched with string {}");
				
				br.close(); 
				return jObject;
			} catch (Exception e) {
				System.out.println("Error while calling ApiDOc REST LOGIN Service");
				e.printStackTrace();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return jObject;
	}
}