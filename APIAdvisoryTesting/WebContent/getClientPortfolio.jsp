<%@page import="com.apidoc.rest.clients.GetClientPortfolioClient"%>
<%@page import="org.json.JSONObject"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<center>
		<%
				String appId=request.getParameter("appId");
				String sessionId=request.getParameter("sessionId");
				String userId=request.getParameter("userId"); 
			%>
		<%=appId %>
		<%=sessionId %>
		<form id="algoForm" name="form" action="getClientPortfolio.jsp"
			class="form-horizontal" method="POST">
			<h3>API Documentation for Fund Expert</h3>
			<br>
			<div class="form-group">
				App ID: <input name="appId" type="text" value="qwertyuiop12345" />
			</div>
			<br>
			<% if(sessionId!=null){ %>
			<div class="form-group">
				Session Id: <input name="sessionId" type="text"
					value="<%=sessionId%>" />
			</div>
			<%}else{ %>
			<div class="form-group">
				Session Id: <input name="sessionId" type="text" value="a48-9afc-c" />
			</div>
			<%} %>
			<br>
			<% if(userId!=null){ %>
			<div class="form-group">
				User Id: <input name="userId" type="text" value="<%=userId%>" />
			</div>
			<%}else{ %>
			<div class="form-group">
				User Id: <input name="userId" type="text"
					value="11e-beaf-25a4b0158e1b" />
			</div>
			<%} %>
			<br>
			<% 
			try{
				if(appId!=null && sessionId!=null && userId!=null)
				{
					GetClientPortfolioClient client=new GetClientPortfolioClient(appId, sessionId, userId);
					JSONObject object=client.run();
					if(object!=null)
					{					
			%>
			<%=object.toString() %>

			<%		}
				}
				
			}catch(Exception e)
			{
				out.println(e.getMessage());
			}	
			%>

			<div class="form-group">
				<div class="col-sm-10">
					<input name="submit" class="btn btn-primary" type="submit" />
				</div>
			</div>

		</form>
		<br>
	</center>
</body>
</html>