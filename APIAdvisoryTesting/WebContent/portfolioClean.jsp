<%@page import="org.json.JSONObject"%>
<%@page import="com.apidoc.rest.clients.PortfolioCleanClient"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Portfolio Clean API</title>
</head>
<body>

	<center>
		<%
				String appId=request.getParameter("appId");
				String sessionId=request.getParameter("sessionId");
				String userId=request.getParameter("userId");
				
				String amfiiCode=request.getParameter("amfiiCode");
				String folioNumber=request.getParameter("folioNumber");
				String date=request.getParameter("date");
			%>
		<%=appId %>
		<%=sessionId %>
		<form id="algoForm" name="form" action="portfolioClean.jsp"
			class="form-horizontal" method="POST">
			<h3>API Documentation for Fund Expert</h3>
			<br>
			<div class="form-group">
				App ID: <input name="appId" type="text" value="qwertyuiop12345" />
			</div>
			<br>
			<% if(sessionId!=null){ %>
			<div class="form-group">
				Session Id: <input name="sessionId" type="text"
					value="<%=sessionId%>" />
			</div>
			<br>
			<%}else{ %>
			<div class="form-group">
				Session Id: <input name="sessionId" type="text" value="a48-9afc-c" />
			</div>
			<br>
			<%} %>
			<br>
			<% if(userId!=null){ %>
			<div class="form-group">
				User Id: <input name="userId" type="text" value="<%=userId%>" />
			</div>
			<br>
			<%}else{ %>
			<div class="form-group">
				User Id: <input name="userId" type="text"
					value="11e-beaf-25a4b0158e1b" />
			</div>
			<br>
			<%} %>
			<br>
			<% if(amfiiCode!=null){ %>
			<div class="form-group">
				amfiiCode: <input name="amfiiCode" type="text"
					value="<%=amfiiCode%>" />
			</div>
			<br>
			<%}else{ %>
			<div class="form-group">
				amfiiCode: <input name="amfiiCode" type="text" value="107578" />
			</div>
			<br>
			<%} %>
			<br>
			<% if(folioNumber!=null){ %>
			<div class="form-group">
				Folio Number: <input name="folioNumber" type="text"
					value="<%=folioNumber%>" />
			</div>
			<br>
			<%}else{ %>
			<div class="form-group">
				Folio Number: <input name="folioNumber" type="text"
					value="54777776/3" />
			</div>
			<br>
			<%} %>
			<br>
			<% if(date!=null){ %>
			<div class="form-group">
				Date: <input name="date" type="text" value="<%=date%>" />
			</div>
			<br>
			<%}else{ %>
			<div class="form-group">
				Date: <input name="date" type="text" value="" />
			</div>
			<br>
			<%} %>
			<br>
			<% 
			try{
				if(appId!=null && sessionId!=null && userId!=null)
				{
					PortfolioCleanClient client=new PortfolioCleanClient(appId, sessionId, userId, amfiiCode, folioNumber, date);
					JSONObject object=client.run();
					if(object!=null)
					{					
			%>
			<%=object.toString() %>

			<%		}
				}
				
				}catch(Exception e)
				{
					out.println(e.getMessage());
				}
			%>

			<div class="form-group">
				<div class="col-sm-10">
					<input name="submit" class="btn btn-primary" type="submit" />
				</div>
			</div>

		</form>
		<br>
	</center>
</body>
</html>