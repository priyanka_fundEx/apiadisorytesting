<%@page import="org.json.JSONObject"%>
<%@page import="com.apidoc.rest.clients.LumpsumInvestClient"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Lumpsum Invest API</title>
</head>
<body>

	<center>
		<%
				String appId=request.getParameter("appId");
				String sessionId=request.getParameter("sessionId");
				String userId=request.getParameter("userId"); 
				String amnt=request.getParameter("amount");
				Double amount=0.0;
				if(amnt!=null)
					amount=Double.parseDouble(amnt); 
			%>
		<%=appId %>
		<%=sessionId %>
		<form id="algoForm" name="form" action="lumpsumInvest.jsp"
			class="form-horizontal" method="POST">
			<h3>API Documentation for Fund Expert</h3>
			<br>
			<div class="form-group">
				App ID: <input name="appId" type="text" value="qwertyuiop12345" />
			</div>
			<br>
			<% if(sessionId!=null){ %>
			<div class="form-group">
				Session Id: <input name="sessionId" type="text"
					value="<%=sessionId%>" />
			</div>
			<br>
			<%}else{ %>
			<div class="form-group">
				Session Id: <input name="sessionId" type="text" value="a48-9afc-c" />
			</div>
			<br>
			<%} %>
			<br>
			<% if(userId!=null){ %>
			<div class="form-group">
				User Id: <input name="userId" type="text" value="<%=userId%>" />
			</div>
			<br>
			<%}else{ %>
			<div class="form-group">
				User Id: <input name="userId" type="text"
					value="11e-beaf-25a4b0158e1b" />
			</div>
			<br>
			<%} %>
			<br>
			<% if(amount!=null){ %>
			<div class="form-group">
				Amount: <input name="amount" type="text" value="<%=amount%>" />
			</div>
			<br>
			<%}else{ %>
			<div class="form-group">
				Amount: <input name="amount" type="text" value="1000" />
			</div> 
			<%} %>
			<br>
			<% 
			try{
				if(appId!=null && sessionId!=null && userId!=null && amount!=null)
				{
					LumpsumInvestClient client=new LumpsumInvestClient(appId, sessionId, userId, amount);
					JSONObject object=client.run();
					if(object!=null)
					{					
			%>
			<%=object.toString() %>

			<%		}
				}
				
				}catch(Exception e)
				{
					out.println(e.getMessage());
				}
			%>

			<div class="form-group">
				<div class="col-sm-10">
					<input name="submit" class="btn btn-primary" type="submit" />
				</div>
			</div>

		</form>
		<br>
	</center>
</body>
</html>