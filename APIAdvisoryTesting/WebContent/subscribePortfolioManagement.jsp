<%@page import="org.json.JSONObject"%>
<%@page
	import="com.apidoc.rest.clients.SubscribePortfolioManagementClient"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Subscribe To Portfolio Management</title>
</head>
<body>

	<center>
		<%
				String appId=request.getParameter("appId");
				String sessionId=request.getParameter("sessionId");
				String userId=request.getParameter("userId"); 
				String noOfMonths=request.getParameter("noOfMonths"); 
			%>
		<%=appId %>
		<%=sessionId %>
		<form id="algoForm" name="form"
			action="subscribePortfolioManagement.jsp" class="form-horizontal"
			method="POST">
			<h3>API Documentation for Fund Expert</h3>
			<br>
			<div class="form-group">
				App ID: <input name="appId" type="text" value="qwertyuiop12345" />
			</div>
			<br>
			<% if(sessionId!=null){ %>
			<div class="form-group">
				Session Id: <input name="sessionId" type="text"
					value="<%=sessionId%>" />
			</div>
			<br>
			<%}else{ %>
			<div class="form-group">
				Session Id: <input name="sessionId" type="text" value="f89-b33d-2" />
			</div>
			<br>
			<%} %>
			<br>
			<% if(userId!=null){ %>
			<div class="form-group">
				User Id: <input name="userId" type="text" value="<%=userId%>" />
			</div>
			<br>
			<%}else{ %>
			<div class="form-group">
				User Id: <input name="userId" type="text"
					value="2e0-915f-341e2f1bca03" />
			</div>
			<br>
			<%} %>
			<br>
			<% if(noOfMonths!=null){ %>
			<div class="form-group">
				No Of Months: <input name="noOfMonths" type="text"
					value="<%=noOfMonths%>" />
			</div>
			<br>
			<%}else{ %>
			<div class="form-group">
				No Of Months: <input name="noOfMonths" type="text" value="12" />
			</div>
			<br>
			<%} %>
			<br>
			<% 
			try{
				if(appId!=null && sessionId!=null && userId!=null && noOfMonths!=null)
				{
					SubscribePortfolioManagementClient client=new SubscribePortfolioManagementClient(appId, sessionId, userId, noOfMonths);
					JSONObject object=client.run();
					if(object!=null)
					{					
			%>
			<%=object.toString() %>

			<%		}
				}
				
				}catch(Exception e)
				{
					out.println(e.getMessage());
				}
			%>

			<div class="form-group">
				<div class="col-sm-10">
					<input name="submit" class="btn btn-primary" type="submit" />
				</div>
			</div>

		</form>
		<br>
	</center>
</body>
</html>