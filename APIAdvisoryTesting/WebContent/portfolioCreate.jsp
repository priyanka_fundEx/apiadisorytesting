<%@page import="org.json.JSONObject"%>
<%@page import="com.apidoc.rest.clients.PortfolioCreateClient"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Portfolio Create API</title>
</head>
<body>
	<center>
		<%
				String appId=request.getParameter("appId");
				String sessionId=request.getParameter("sessionId");
				String userId=request.getParameter("userId");  

				String amfiiCode=request.getParameter("amfiiCode");
				String optionType=request.getParameter("optionType");
				String folioNumber=request.getParameter("folioNumber"); 
				String nav=request.getParameter("nav"); 
				String units=request.getParameter("units");
				String action=request.getParameter("action");
				String date=request.getParameter("date"); 
			
			%>
		<%=appId %>
		<%=sessionId %>
		<form id="algoForm" name="form" action="portfolioCreate.jsp"
			class="form-horizontal" method="POST">
			<h3>API Documentation for Fund Expert</h3>
			<br>
			<div class="form-group">
				App ID: <input name="appId" type="text" value="qwertyuiop12345" />
			</div>
			<br>
			<% if(sessionId!=null){ %>
			<div class="form-group">
				Session Id: <input name="sessionId" type="text"
					value="<%=sessionId%>" />
			</div>
			<%}else{ %>
			<div class="form-group">
				Session Id: <input name="sessionId" type="text" value="f89-b33d-2" />
			</div>
			<%} %>
			<br>
			<% if(userId!=null){ %>
			<div class="form-group">
				User Id: <input name="userId" type="text" value="<%=userId%>" />
			</div>
			<%}else{ %>
			<div class="form-group">
				User Id: <input name="userId" type="text"
					value="2e0-915f-341e2f1bca03" />
			</div>
			<%} %>
			<br>
			<% if(amfiiCode!=null){ %>
			<div class="form-group">
				Amfii Code: <input name="amfiiCode" type="text"
					value="<%=amfiiCode%>" />
			</div>
			<%}else{ %>
			<div class="form-group">
				Amfii Code: <input name="amfiiCode" type="text" value="100848" />
			</div>
			<%} %>
			<br>
			<% if(folioNumber!=null){ %>
			<div class="form-group">
				Folio Number: <input name="folioNumber" type="text"
					value="<%=folioNumber%>" />
			</div>
			<%}else{ %>
			<div class="form-group">
				Folio Number: <input name="folioNumber" type="text"
					value="53674776/3" />
			</div>
			<%} %>
			<br>

			<% if(optionType!=null){ %>
			<div class="form-group">
				Option Type: <input name="optionType" type="text"
					value="<%=optionType%>" />
			</div>
			<%}else{ %>
			<div class="form-group">
				Option Type: <input name="optionType" type="text" value="GROWTH" />
			</div>
			<%} %>
			<br>

			<% if(nav!=null){ %>
			<div class="form-group">
				NAV: <input name="nav" type="text" value="<%=nav%>" />
			</div>
			<%}else{ %>
			<div class="form-group">
				NAV: <input name="nav" type="text" value="50.4" />
			</div>
			<%} %>
			<br>

			<% if(units!=null){ %>
			<div class="form-group">
				Units: <input name="units" type="text" value="<%=units%>" />
			</div>
			<%}else{ %>
			<div class="form-group">
				Units: <input name="units" type="text" value="150.99" />
			</div>
			<%} %>
			<br>

			<% if(action!=null){ %>
			<div class="form-group">
				Action: <input name="action" type="text" value="<%=action%>" />
			</div>
			<%}else{ %>
			<div class="form-group">
				Action: <input name="action" type="text" value="INVEST" />
			</div>
			<%} %>
			<br>
			<% if(date!=null){ %>
			<div class="form-group">
				Date: <input name="date" type="text" value="<%=date%>" />
			</div>
			<%}else{ %>
			<div class="form-group">
				Date: <input name="date" type="text" value="2015-11-05" />
			</div>
			<%} %>
			<br>
			<% 
			try{
				if(appId!=null && sessionId!=null && userId!=null && folioNumber!=null && amfiiCode!=null && 
						optionType!=null && nav!=null && action!=null && units!=null && date!=null)
				{
					PortfolioCreateClient client=new PortfolioCreateClient(appId, sessionId, userId, amfiiCode, folioNumber, 
													optionType, action, nav, units, date);
					JSONObject object=client.run();
					if(object!=null)
					{					
			%>
			<%=object.toString() %>

			<%		}
				}
				
				}catch(Exception e)
				{
					out.println(e.getMessage());
				}
			%>

			<div class="form-group">
				<div class="col-sm-10">
					<input name="submit" class="btn btn-primary" type="submit" />
				</div>
			</div>

		</form>
		<br>
	</center>
</body>
</html>