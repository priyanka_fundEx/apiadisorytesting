<%@page import="org.json.JSONObject"%>
<%@page import="javax.validation.ValidationException"%>
<%@page import="com.apidoc.rest.clients.LoginClient"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<center>
		<%
				String appId=request.getParameter("appId");
				String secretKey=request.getParameter("secretKey");
		%>
		<%=appId %>
		<%=secretKey %>
		<form id="algoForm" name="form" action="login.jsp"
			class="form-horizontal" method="POST">
			<h3>API Documentation for Fund Expert</h3>
			<br>
			<div class="form-group">
				App ID: <input name="appId" type="text" value="qwertyuiop12345" required/>
			</div>
			<br>

			<div class="form-group">
				Secret Key: <input name="secretKey" type="text" value="mnbvcxz09876" required/>
			</div>
			<br> <br>
			<% try{
				if(appId!=null && secretKey!=null)
				{
					LoginClient client=new LoginClient(appId, secretKey);
					JSONObject obj=client.run();
					System.out.println("Object is: "+obj);
					//if(obj.length()==0 && !obj.toString().equals("{}"));
					if(!obj.equals("{}"))
					{
						if(obj.has("sessionId"))
						{
			%>
			<div class="form-group">
				Session ID: <input name="sessionId" type="text"
					value="<%=obj.getString("sessionId")%>" /><br>
					<% }
					   else
						{%>
							<%=obj %>
			</div><br>  
			<%			}
					}  
						
				}
			}catch(Exception e)
			{
				out.println(e.getMessage());
				e.printStackTrace();
			} %>
<br>
			<div class="form-group">
				<div class="col-sm-10">
					<input name="submit" class="btn btn-primary" type="submit" />
				</div>
			</div>

		</form>
		<br> 
	</center>
</body>
</html>